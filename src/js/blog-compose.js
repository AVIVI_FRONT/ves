/* jshint jquery: true */
/* global Quill */

'use strict';

$(function() {

$('.blog-compose-add-tag').on('keydown', function(e) {
	if(e.key !== 'Enter') return;
	blogComposeAddTag(this.value);
});
$('.blog-compose-add-tag-btn').click(function(e) {
	blogComposeAddTag($('.blog-compose-add-tag').val());
});
$('.blog-compose-added-tags').on('click', '.blog-compose-remove-tag-btn', function() {
	$(this).closest('.blog-compose-tag').remove();
	updatePreview();
});
$('.blog-compose-popular-tags').on('click', '.blog-compose-insert-tag', function() {
	blogComposeAddTag(this.textContent.slice(1));
	$(this).remove();
});

function blogComposeAddTag(tag) {
	$('.blog-compose-added-tags').append(`<div class="blog-compose-tag">${tag} <button class="blog-compose-remove-tag-btn has-tooltip" aria-label="Удалить тег"><i class="fas fa-times"></i></button><input type="hidden" name="tags[]" value="${tag}"></div>`);
	updatePreview();
}


const quill = new Quill('#editor', {
	theme: 'snow',
	placeholder: 'Текст',
	modules: {
		toolbar: [
			[{ 'header': [2, 3, 4, false] }],
			['bold', 'italic', 'underline', 'strike'],        // toggled buttons
			[{ 'list': 'ordered'}, { 'list': 'bullet' }],
			[{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
			[{ 'align': '' }, {'align': 'right'}, {'align': 'center'}, {'align': 'justify'}],
			['clean'] ,
		]
	}
});


const updatePreview = () => {
	const postTitle = document.querySelector('.blog-compose-post-title').value;
	const contentHTML = document.querySelector('.blog-compose-editor .ql-editor').innerHTML;
	const tagsHTML = [...document.querySelectorAll('.blog-compose-added-tags .blog-compose-tag')].map(tag => '<a href="" class="tag">' + tag.textContent.trim() + '</a>').join('');
	document.querySelector('.blog-compose-preview').innerHTML = `
			<h1 class="post-title">${postTitle}</h1>
			<div class="post-image-wrap"><img src="image/blog-post-image.jpg" alt="" class="post-image"></div>
			<div class="post-content">${contentHTML}</div>
			<footer class="post-footer">
				<div class="post-tags">${tagsHTML}</div>
			</footer>`;
};


quill.on('text-change', updatePreview);
$('.blog-compose-post-title').on('change', updatePreview);
updatePreview();

});
