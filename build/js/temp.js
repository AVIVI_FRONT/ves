// This is temporary js script. It is mainly used to populate certain blocks with html, and simulate user interaction events.

/* jshint jquery: true */

'use strict';

$(function() {
	let $posts = $('.posts-list').eq(0).children();
	if($posts.length > 2) {
		$posts = $posts.slice(1);
	}


	$('.carousel-list').each(function() {
		for(let i = 0; i < 18; i++) {
			$(this).append($(this).find('li:last-child').clone());
		}
	});
	$('.grid-list').each(function() {
		for(let i = 0; i < 5; i++) {
			$(this).append($(this).find('li:last-child').clone());
		}
	});
	$(document).on('click', 'a[href=""]', function(e) {
		e.preventDefault();
	});
	$('form').on('submit', function(e) {
		e.preventDefault();
	});
	// for(let i = 0; i < 5; i++) {
	// 	document.querySelector('.reviews-list').append(document.querySelector('.review:last-child').cloneNode(true));
	// }
	// for(let i = 0; i < 3; i++) {
	// 	document.querySelector('.news-list').append(document.querySelector('.news-story:last-child').cloneNode(true));
	// }
	// for(let i = 0; i < 4; i++) {
	// 	document.querySelector('.on-the-net-list').append(document.querySelector('.on-the-net-list > li:last-child').cloneNode(true));
	// }
	// for(let i = 0; i < 4; i++) {
	// 	document.querySelector('.new-books-list').append(document.querySelector('.new-books-list > li:last-child').cloneNode(true));
	// }


	$('.page-reviews .reviews-list').each(function() {
		$(this).html($(this).html().repeat(3));
	});

	$('.js-load-more-btn').click(function() {
		this.classList.add('loading');
		this.disabled = true;
		setTimeout(() => {
			const $newPosts = $posts.clone().addClass('just-loaded-post');
			$(this).removeAttr('disabled').removeClass('loading').closest('.load-more-wrap').prev('.posts-list').append($newPosts);
			$newPosts[0].scrollIntoView();
		}, 1000);
	});

	$('.user-friends-list').first().each(function() {
		const html = $(this).html();
		for(let i = 2; i < 5; i++) {
			$(this).append(html.replace('friend-dropdown-btn-1', `friend-dropdown-btn-${i}`));
		}
	});

	$('.add-to-cart-btn').click(function(e) {
		e.preventDefault();

		this.disabled = true;
		$(this).prepend('<i class="fas fa-circle-notch fa-spin"></i> ');
		setTimeout(() => {
			openDialog(document.querySelector('#added-to-cart-dialog'));
			this.disabled = false;
			$(this).find('.fa-circle-notch').remove();
		}, 1000);
	});

	$('.post-like-btn').click(function() {
		this.classList.toggle('liked');
	});

	$('.add-to-fav-btn').click(function() {
		this.classList.toggle('added');
		this.innerHTML = '<i class="icon-circle bg-prim fas fa-heart"></i> В избранном';
	});

	if(document.body.classList.contains('page-category')) {
		let cats;
		const catTemplateHTML = document.querySelector('.category-cat-list').innerHTML;

		const updatePage = () => {
			const cat = decodeURIComponent(location.hash.slice(1));
			let submenu = cats.filter(c => c.name === cat);
			if(!submenu.length) return alert(`Категории ${cat} не существует`);
			submenu = submenu[0].submenu;
			document.querySelector('.category-cat-list').innerHTML = submenu.reduce((ret, subcat) => {
				if(typeof subcat != 'object') {
					subcat = {
						name: subcat,
						submenu: [],
					};
				}
				const link = `catalog.html#${cat}/${subcat.name}`;

				return ret + catTemplateHTML
					.replace(/{category link}/g, link)
					.replace(/{category name}/g, subcat.name)
					.replace('{category image}', `${subcat.name.replace(/ /g, '-')}.png`)
					.replace('{category submenu}', subcat.submenu.reduce((ret, itm) => ret + `<li class="category-cat-submenu-item"><a class="link-clr-fg" href="${link + '/' + itm}">${itm}</a>`, ''));
			}, '');
			document.querySelector('.category-heading').textContent = cat;
			if(cat.length > 17) {
				$('.category-heading').addClass('category-heading-small');
			} else {
				$('.category-heading').removeClass('category-heading-small');
			}
			document.querySelector('.category-header').setAttribute('style', `background-image: url("image/category/banner-${cat.replace(/ /g, '-')}.jpg");`);
			window.scrollTo(0, 0);
		};

		fetch('data/categories.json').then(response => {
			response.json().then(data => {
				cats = data;
				updatePage();
			});
		}, err => {
			alert('Не удалось загрузить категории');
		});

		window.addEventListener('hashchange', updatePage);
	}


	// Simulate navigation on Categories and Catalog pages
	if(document.querySelector('.full-cats-menu')) {
		window.addEventListener('hashchange', updateLocation);
		updateLocation();

		function updateLocation() {
			const pathStr = decodeURIComponent(location.hash.slice(1));
			if(!location.hash) return;
			const path = pathStr.split('/');
			let link = path.length === 1 ? 'category.html' : 'catalog.html';

			document.title = path[path.length - 1];
			document.querySelector('.breadcrumb').innerHTML = `
				<li class="breadcrumb-item"><a href=".">Главная<span class="min-screen-xs"> страница</span></a></li>
			` + path.map((loc, i) => {
				if(i == path.length - 1) {
					return `<li class="breadcrumb-item active" aria-current="page">${loc}</li>`;
				}
				return `<li class="breadcrumb-item"><a href="${(i === 0 ? 'category.html' : 'catalog.html') + '#' + path.slice(0, i + 1).join('/')}">${loc}</a></li>`;
			}).join('');

			link += '#' + path.join('/');
			link = document.querySelector(`.full-cats-menu-link[href="${link}"]`);
			if(!link) return alert(`Категории ${pathStr} не существует`);
			$('.full-cats-menu-item.active-parent').removeClass('active-parent');
			$('.full-cats-menu-link.active').removeClass('active');
			link.classList.add('active');
			$(link).closest('.full-cats-menu > .full-cats-menu-item').addClass('active-parent');
			$(link).parents('.full-cats-menu-item').each((i, menu) => {
				$(menu).addClass('expanded').children('.full-cats-toggle').attr('aria-expanded', true);
			});
		}
	}


	$('[data-toggle="dialog"][data-target="#author-ask-dialog"]').click(function() {
		this.dataset.target = this.dataset.target === '#author-ask-dialog' ? '#author-ask-dialog-not-authorized' : '#author-ask-dialog';
	});

	$('.blog-compose-form').on('submit', function() {
		openDialog(document.querySelector('#blog-post-published-dialog'));
	});

	$('.chat-load-messages-btn').click(function() {
		$('.chat-messages').html($('.chat-messages').html().repeat(2));
	});
});
