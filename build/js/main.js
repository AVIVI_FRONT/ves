/* jshint jquery: true */
/* global Swiper */

'use strict';


{
const browser = {
	sizes: {
		xs: 480,
		sm: 768,
		md: 992,
		lg: 1200,
	},

	getSize() {
		const w = window.innerWidth;
		const sizes = this.sizes;

		if(w < sizes.xs) return 'xs';
		else if(w < sizes.xs) return 'sm';
		else if(w < sizes.xs) return 'md';
		else return 'lg';
	}
};


const isScrolledIntoView = (el) => {
	const {top, bottom} = el.getBoundingClientRect();

	// Partially visible elements return true:
	return top < window.innerHeight && bottom >= 0;
};



const offsetAllCarousels = ()  =>{
	let id = 1;
	$('.carousel-cont').each(function() {
		this.querySelector('.carousel-prev-btn').setAttribute('aria-controls', 'carousel-' + id);
		this.querySelector('.carousel-next-btn').setAttribute('aria-controls', 'carousel-' + id);
		this.querySelector('.carousel-viewport').id = 'carousel-' + id;
		id += 1;
		this.querySelector('.carousel-viewport').setAttribute('aria-live', 'polite');
		offsetCarousel(this);
	});
};


const offsetCarousel = (carEl, diff) => {
	let index = + carEl.dataset.index || 0;
	const list = carEl.querySelector('.carousel-list');
	index += (diff || 0);
	const itemSize = list.firstElementChild.getBoundingClientRect().width + parseInt(getComputedStyle(list)['grid-column-gap'], 10);
	const viewPort = carEl.querySelector('.carousel-viewport');
	const viewBox = viewPort.getBoundingClientRect();
	const maxIndex = Math.floor((list.scrollWidth - viewBox.width) / itemSize);
	if(maxIndex <= 0) {
		carEl.querySelector('.carousel-prev-btn').style.display = 'none';
		carEl.querySelector('.carousel-next-btn').style.display = 'none';
		return;
	}
	index = Math.max(0, Math.min(maxIndex, index));
	carEl.dataset.index = index;
	carEl.querySelector('.carousel-prev-btn').disabled = index === 0;
	carEl.querySelector('.carousel-next-btn').disabled = index === maxIndex;

	const offset = - index * itemSize + 'px';
	list.style.left = offset;
	[...carEl.querySelectorAll('.carousel-list-item')].forEach(item => item.classList.remove('invisible'));

	list.addEventListener('transitionend', function() {
		[...carEl.querySelectorAll('.carousel-list-item')].forEach(item => {
			const {left, right} = item.getBoundingClientRect();
			const invisible = right < viewBox.left + 5 || left > viewBox.right - 5;
			item.classList.toggle('invisible', invisible);
			item.setAttribute('aria-hidden', invisible);
		});
	}, {once: true});
};


if(CSS.supports('display', 'grid')) {
	document.body.classList.remove('no-support');
	document.querySelector('.js-update-browser').remove();
}



const onScroll = () => {
	if(window.scrollY >= 120) {
		document.body.classList.add('page-scrolled');
	} else {
		document.body.classList.remove('page-scrolled');
	}
};


const resize = () => {
	let scrolled = false;
	if($('body').hasClass('page-scrolled')) {
		scrolled = true;
		$('body').removeClass('page-scrolled');
	}

	$('.site-header, .site-header-top, .menu-group, .top-nav').css('transition', 'none');
	const headerHeight = $('.site-header').height();
	$('.site-content').css('margin-top', headerHeight);

	if(scrolled) $('body').addClass('page-scrolled');
	// Repaint
	$('.site-header').height();
	$('.site-header, .site-header-top, .menu-group, .top-nav').css('transition', '');
};


$(window).on('resize', offsetAllCarousels);
$(window).on('resize', resize);
$(window).on('load', function() {
	setTimeout(() => {
		resize();
		// TODO: This is here to run after temp.js
		offsetAllCarousels();
	}, 200);
});

onScroll();
document.addEventListener('scroll', onScroll);



jQuery(function($) {
	// Home page slider
	if(document.body.classList.contains('page-home')) {
		new Swiper(".swiper-container", {
			autoplay: false,
			loop: true,

			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
			a11y: {
				paginationBulletMessage: 'К слайду {{index}}'
			},
		});
	}

	$('.carousel-prev-btn').click(function() {
		offsetCarousel(this.closest('.carousel-cont'), -1);
	});
	$('.carousel-next-btn').click(function() {
		offsetCarousel(this.closest('.carousel-cont'), 1);
	});

    $('.js-toggle-text').click(function(){
        var hBlock = $('.author-about-content-cont');
        var value = (hBlock.is('.expanded') ? 'Развернуть' : 'Свернуть');
        $(this).attr('aria-label', value);
    });

	// About us slider on home and about us pages
	const aboutUsSlider = {
		timer: null,
		interval: 5000,

		init() {
			if(!document.querySelector('.about-us-carousel')) return;
			$('.about-us-carousel').click(this.next);

			document.addEventListener('scroll', aboutUsSlider._onScroll);
			this._onScroll();
		},

		reset() {
			if(!this.timer) return;
			clearTimeout(this.timer);
			this.timer = null;
		},

		start() {
			if(this.timer) return;
			this.reset();
			$('.about-us-carousel .slide').removeClass('current previous');
			$('.about-us-carousel .slide').eq(0).addClass('current');
			this.timer = setTimeout(this.next, this.interval);
		},

		next() {
			const $prev = $('.about-us-carousel .slide.current');
			let $next = $prev.next();
			if($next.length === 0) $next = $('.about-us-carousel .slide').eq(0);
			$prev.removeClass('current');
			$('.about-us-carousel .slide.previous').removeClass('previous');
			$prev.addClass('previous');
			$next.addClass('current');
			clearTimeout(aboutUsSlider.timer);
			aboutUsSlider.timer = setTimeout(aboutUsSlider.next, aboutUsSlider.interval);
		},

		_onScroll() {
			if(isScrolledIntoView(document.querySelector('.about-us-carousel'))) {
				aboutUsSlider.start();
			} else {
				aboutUsSlider.reset();
			}
		},
	};

	aboutUsSlider.init();



	// Wrap text in span's
	$('.side-text:not(.fixed-font)').each(function() {
		const text = `<span class="sr-only">${this.textContent}</span>`;
		$(this)
			.find('span')
				.each(function() {
					$(this).attr({
						class: 'section-heading-line',
						'aria-hidden': true,
					}).html(spanize(this));
				}).end()
			.find('.section-heading').prepend(text);
	});

	function spanize(el) {
		return [...el.textContent].map(l => `<span>${l === ' ' ? '' : l}</span>`).join('');
	}


	class ToggleButton {
		constructor($btn, target, opts = {bodyClass: false}) {
			this.target = target;
			this.$target = $(target);
			this.$heightTarget = this.$target.find('.expandable-menu-wrap');
			if(!this.$heightTarget.length) this.$heightTarget = this.$target;
			this.$btn = $($btn);
			this.$target.off('transitionend');
			this.$btn.click(this.toggle.bind(this));
			this.$target[0].toggleTarget = this;
			this.opts = opts;
		}

		open() {
			const $btn = this.$btn;
			const $target = this.$target;
			const $heightTarget = this.$heightTarget;
			$btn.attr('aria-pressed', true);
			$heightTarget.css({height: 'auto', 'max-height': 'auto'});
			$target.addClass('menu-expanded');
			const h = $heightTarget.height();
			$heightTarget.css({height: '', 'max-height': ''});
			$target.removeClass('menu-expanded');
			$heightTarget.height(h).one('transitionend', function() {
				this.style.height = 'auto';
			});
			$target.addClass('menu-expanded');
			if(this.opts.bodyClass) document.body.dataset.expandedMenu = '';
		}

		close() {
			this.$btn.attr('aria-pressed', false);
			this.$heightTarget.height(this.$heightTarget.height());
			this.$heightTarget.height();
			this.$heightTarget.one('transitionend', function() {
				this.style.height = '';
			});
			this.$target.removeClass('menu-expanded')
				.find('.menu-expanded').each(function() {
					if(this.toggleTarget) {
						this.toggleTarget.close();
					}
				});
			if(this.opts.bodyClass) delete document.body.dataset.expandedMenu;
		}

		toggle() {
			if(this.$target.hasClass('menu-expanded')) {
				this.close();
			} else {
				this.open();
			}
		}
	}

	// Toggle buttons
	new ToggleButton('#header-toggle', '#site-header', {bodyClass: true});
	new ToggleButton('.cat-menu-toggle', '.cat-menu-nav');
	if(document.querySelector('.full-cats-menu-toggle')) new ToggleButton('.full-cats-menu-toggle', '#full-cats-menu');
	if(document.querySelector('.catalog-filter-toggle')) new ToggleButton('.catalog-filter-toggle', '#catalog-filter-cont');

	$('.cat-menu-expand-btn').click(function(e) {
		e.preventDefault();
		$('.cat-menu-nav').toggleClass('menu-expanded');
		$(this).find('.cat-menu-expand-btn-text').text($('.cat-menu-nav').hasClass('menu-expanded') ? 'Свернуть' : 'Показать всё');
	});



	// Header search area
	$('.js-search-btn').click(function(e) {
		if(window.innerWidth < browser.sizes.md && !$('.site-header').hasClass('search-expanded')) {
			e.preventDefault();
			$('.site-header').addClass('search-expanded');
			$('.js-search').focus();
		}
	});
	$('.search').blur(function(e) {
		if(window.innerWidth < browser.sizes.md) {
			this.value = '';
			$('.site-header').removeClass('search-expanded');
		}
	});


	$('.js-personal-nav').on('menuopen', function() {
		$('body').addClass('personal-menu-expanded');
	});
	$('.js-personal-nav').on('menuclose', function() {
		$('body').removeClass('personal-menu-expanded');
	});

	const updatePersonalInfoForm = () => {
		$('.personal-info-selected-interests').html(
			[...document.querySelectorAll('.personal-interests-list input[type=checkbox]:checked + label')]
				.map(el => `<span>${el.textContent}</span>`)
				.join(', ')
		);
	};

	updatePersonalInfoForm();
	$('.personal-interests-list').on('change', updatePersonalInfoForm);



	$('.js-scroll-up-btn').click(function() {
		window.scrollTo(0, 0);
	});



	$('.js-expandable-content-expand-btn').click(function() {
		$(this).closest('.expandable-content').toggleClass('expanded');
	});



	// Book description accordion
	$('.book-description-accordion').accordion({
		heightStyle: "content",
	});



	// Make password visible
	$('.js-show-password-btn').click(function() {
		const $target = $(this.dataset.target);
		if($target.attr('type') === 'password') {
			$target.attr('type', 'text');
			$(this).attr('aria-label', 'Скрыть пароль').html('<i class="far fa-eye">');
		} else {
			$target.attr('type', 'password');
			$(this).attr('aria-label', 'Показать пароль').html('<i class="far fa-eye-slash">');
		}
	});



	// Categories navigation on Catalog and Categories pages
	$('.full-cats-menu').on('click', '.js-full-cats-toggle', function(e) {
		const $menu = $(e.target).closest('.full-cats-menu-item');

		toggle($menu);

		function setHeight($menu) {
			$menu = $($menu);
			const wasExpanded = $menu.hasClass('expanded');
			const $submenu = $menu.children('.full-cats-submenu');
			$submenu.css({height: ''});
			$menu.addClass('expanded');
			$submenu.css('height', $submenu[0].offsetHeight);
			if(!wasExpanded) {
				$menu.removeClass('expanded');
				$submenu.height();
			}
			$submenu.one('transitionend', function() {
				this.style.height = '';
			});
		}


		function toggle($menu) {
			$menu = $($menu);
			if($menu.hasClass('expanded')) close($menu);
			else open($menu);
		}


		function open($menu) {
			$menu = $($menu);

			$('.full-cats-menu .full-cats-menu-item.expanded').each((i, el) => {
				if($(el).find($menu).length) return;
				close(el);
			});

			setHeight($menu);
			$menu.addClass('expanded');
			$menu.children('.js-full-cats-toggle').attr('aria-expanded', true);
		}


		function close($menu) {
			$menu = $($menu);
			const $submenu = $menu.children('.full-cats-submenu');
			if(!$submenu.length) return;
			$submenu.css('height', $submenu[0].offsetHeight);
			$submenu[0].offsetHeight;
			$submenu.one('transitionend', function() {
				this.style.height = '';
				$submenu.find('.full-cats-menu-item').each((i, submenu) => close(submenu));
			});
			$menu.removeClass('expanded');
			$menu.children('.js-full-cats-toggle').attr('aria-expanded', false);
		}
	});



	// Page User
	$('.js-expandable-content-multi-expand-btn').click(function() {
		const $container = $(this).closest('.expandable-content');

		if($container.hasClass('expanded')) {
			$container.removeClass('expanded');
			$container.find('.js-expandable-content-multi-expand-btn').removeAttr('aria-expanded').attr('aria-label', 'Развернуть');
		} else {
			$container.addClass('expanded');
			$container.find('.js-expandable-content-multi-expand-btn').attr({'aria-label': 'Свернуть', 'aria-expanded': 'true'});
		}
	});
});



$(window).on('load', () => {
	// Book images gallery
	const galleryThumbs = new Swiper('.book-gallery-thumbs', {
		spaceBetween: 10,
		slidesPerView: 4,
		freeMode: true,
		watchSlidesVisibility: true,
		watchSlidesProgress: true,
	});
	const galleryTop = new Swiper('.book-gallery-top', {
		spaceBetween: 10,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		thumbs: {
			swiper: galleryThumbs
		},
		pagination: {
			el: '.swiper-pagination',
			clickable: true,
		},
		a11y: {
			prevSlideMessage: 'Предыдущая картинка',
			nextSlideMessage: 'Следующая картинка',
		},
	});
	const bestBookSlider = new Swiper('.catalog-best-book-carousel', {
		direction: 'vertical',
		slidesPerView: 1,
		allowTouchMove: false,
		navigation: {
			nextEl: '.catalog-best-book-carousel-next',
			prevEl: '.catalog-best-book-carousel-prev',
		},
		a11y: {
			prevSlideMessage: 'Предыдущий слайд',
			nextSlideMessage: 'Следующий слайд',
		},
	});



	// Messages and notifications
	$('.js-open-messages-btn').click(function(e) {
		e.preventDefault();
		$('.js-chat-dialog')
			.removeClass('chat-dialog-notifications')
			.addClass('chat-dialog-messages')
			.attr('aria-labelledby', 'chat-dialog-label-messages');
		if(window.innerWidth >= 992) {
			$('.js-chat-dialog').addClass('chat-dialog-chat-open');
			$('.chats-list-item:first-child .chats-list-item-link').addClass('selected');
		}
	});
	$('.js-open-notifications-btn').click(function(e) {
		e.preventDefault();
		$('.js-chat-dialog')
			.removeClass('chat-dialog-messages chat-dialog-chat-open')
			.addClass('chat-dialog-notifications')
			.attr('aria-labelledby', 'chat-dialog-label-notifications');
		$('.chats-list-item-link.selected').removeClass('selected');
	});
	$('.js-chats-open-notifications-btn').click(function() {
		$('.js-chat-dialog')
			.removeClass('chat-dialog-messages')
			.addClass('chat-dialog-notifications')
			.attr('aria-labelledby', 'chat-dialog-label-notifications');
		$('.chats-list-item-link.selected').removeClass('selected');
	});

	$('.chats-list-item-link').click(function() {
		$('.js-chat-dialog')
			.removeClass('chat-dialog-notifications')
			.addClass('chat-dialog-messages chat-dialog-chat-open')
			.attr('aria-labelledby', 'chat-dialog-label-messages');
		$('.chats-list-item-link.selected').removeClass('selected');
		$(this).addClass('selected');
		$('.js-chat-compose-text').focus();
	});

	$('.js-chat-toggle-msg-list-btn').click(function() {
		$('.js-chat-dialog')
			.removeClass('chat-dialog-notifications')
			.addClass('chat-dialog-messages')
			.attr('aria-labelledby', 'chat-dialog-label-messages');
	});
	$('.js-chat-messages-back-btn').click(function() {
		$('.js-chat-dialog').removeClass('chat-dialog-chat-open');
		$('.chats-list-item-link.selected').removeClass('selected');
	});



	// Skip to content link
	$('.skip-to-main-link').click(function(e) {
		e.preventDefault();
		$('.site-content').focus();
	});



	// Rate stars accessibility
	$('.rate').on('keydown', function(e) {
		if(e.target.type !== 'radio') return;

		if(!['ArrowLeft', 'ArrowRight', 'ArrowUp', 'ArrowDown'].includes(e.key)) return;

		e.preventDefault();

		const items = [...this.querySelectorAll('input[type="radio"]')];
		if(items.length === 0) return;
		let index = items.indexOf(e.target);

		if(e.key === 'ArrowRight' || e.key === 'ArrowUp') {
			index--;
		} else if((e.key === 'ArrowLeft' || e.key === 'ArrowDown') && index < items.length - 1) {
			index++;
		}

		if(index < 0) index = 0;

		items[index].focus();
	});

    // mail validation
    function validate(){
        //custom validation rule
        $.validator.addMethod("customemail",
            function(value, element) {
                return /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(value);
            },
            "Пожалуйста, введите корректный адрес электронной почты."
        );
        $(".subscribe-form").validate({
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.insertBefore(element);
            },
            rules: {
                email: {
                    required:  {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
                    customemail: true
                },
            },
            submitHandler: function(form) {
                // form.submit();
                $('#popup-subs').addClass('open');
            }
        });
        $(".contact-form").validate({
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.insertBefore(element);
            },
            rules: {
                message: {
                    required: true
                },
                name: {
                    required: true
                },
                email: {
                    required:  {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
                    customemail: true
                },
            },
            submitHandler: function(form) {
                // form.submit();
                $('#popup-thx').addClass('open');
            }
        });
        $(".book-form").validate({
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.insertBefore(element);
            },
            rules: {
                email: {
                    required:  {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
                    customemail: true
                },
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
        $(".news-subscribe-form").validate({
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.insertBefore(element);
            },
            rules: {
                email: {
                    required:  {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
                    customemail: true
                },
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
        $(".become-author-form").validate({
            errorElement: 'span',
            rules: {
                email: {
                    required:  {
                        depends:function(){
                            $(this).val($.trim($(this).val()));
                            return true;
                        }
                    },
                    customemail: true
                },
                name: {
                    required: true
				},
                tel: {
                    required: true
                },
                message: {
                    required: true
                },
                attachement: {
                    required: true
                },
            },
            messages: {
                attachement: "Необходимо выбрать файл"
			},
            submitHandler: function(form) {
                form.submit();
            }
        });
    }
    validate();

    // popup's close
	function popupClose() {
		var btn = $('.pop .close-dialog');
		btn.on('click',function () {
			$(this).parents('.pop').removeClass('open');
        })
    }
    popupClose();
});
}
